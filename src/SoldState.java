
public class SoldState implements State {

	@Override
	public void insertQuarter(VendingMachine machine) {
		System.out.println("���������, ����� ��������");
	}

	@Override
	public void ejectQuarter(VendingMachine vendingMachine) {
		System.out.println("�� ��� ������� �����, ������ ����������");
	}

	@Override
	public void dispense(VendingMachine vendingMachine) {
		System.out.println("����� ��������...");
		vendingMachine.count = vendingMachine.count - 1;
		if (vendingMachine.count == 0) {
			System.out.println("������ �����������!");
			vendingMachine.setState(new SoldOutState());
		} else {
			vendingMachine.setState(new NoCoinState());
		}
	}
	
	@Override
	public void pushSelectButton(VendingMachine vendingMachine) {
		System.out.println("������� �������");
	}

}

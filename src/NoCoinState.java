
public class NoCoinState implements State {

	@Override
	public void insertQuarter(VendingMachine machine) {
		machine.setState(new HasCoinState());
		System.out.println("�� ��������� ������");
	}

	@Override
	public void ejectQuarter(VendingMachine vendingMachine) {
		System.out.println("�� �� ��������� ������");
	}

	@Override
	public void dispense(VendingMachine vendingMachine) {
		System.out.println("������ ���������� ���������� ������");
	}
	
	@Override
	public void pushSelectButton(VendingMachine vendingMachine) {
		System.out.println("�� ��������� ������� �����, �� �������� ������");
	}

}
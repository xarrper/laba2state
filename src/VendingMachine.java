public class VendingMachine {
 
	State state = new SoldOutState();
	int count = 0;
  
	public VendingMachine(int count) {
		this.count = count;
		if (count > 0) {
			state = new NoCoinState();
		}
	}
	
	public void setState(State s) {
		state = s;
	}
	
	public void insertCoin() {
		state.insertQuarter(this);
	}
	
	public void ejectCoin() {
		state.ejectQuarter(this);
	}
	
	public void pushSelectButton() {
		state.pushSelectButton(this);
	}
	
	public void  dispense() {
		state.dispense(this);
	}
	
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("\n�������� �������");
		result.append("� �������: " + count + " �������");
		result.append("\n���������: ");
		if (state instanceof SoldOutState) {
			result.append("������ ����������");
		} else if (state instanceof NoCoinState) {
			result.append("�������� ������");
		} else if (state instanceof HasCoinState) {
			result.append("�������� ������ ������");
		} else if (state instanceof SoldState) {
			result.append("����� ��������");
		}
		result.append("\n");
		return result.toString();
	}
}
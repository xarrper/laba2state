interface State {
	void insertQuarter(VendingMachine machine);

	void ejectQuarter(VendingMachine gumballMachine);

	void dispense(VendingMachine gumballMachine);

	void pushSelectButton(VendingMachine gumballMachine);
}
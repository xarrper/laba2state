
public class HasCoinState implements State {

	@Override
	public void insertQuarter(VendingMachine machine) {
		System.out.println("������ ��������� ������ ����� ������");
	}

	@Override
	public void ejectQuarter(VendingMachine vendingMachine) {
		System.out.println("������ ����������");
		vendingMachine.setState(new NoCoinState());
	}

	@Override
	public void dispense(VendingMachine vendingMachine) {
		System.out.println("����� �� �����");
	}
	
	@Override
	public void pushSelectButton(VendingMachine vendingMachine) {
		System.out.println("����� ������...");
		vendingMachine.setState(new SoldState());
		vendingMachine.state.dispense(vendingMachine); 
	}

}
